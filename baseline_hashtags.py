import argparse
import gensim
import time
import random
import math
import ast
from kmedian_clustering import KMedian
from sklearn.feature_extraction.text import TfidfVectorizer,TfidfTransformer,HashingVectorizer
from sklearn.pipeline import make_pipeline

fixedseed=24011993
random.seed(fixedseed)

parser = argparse.ArgumentParser()
parser.add_argument("--train_stream",
    help="file containing the stream of tweets to use to set up vocabulary and tfidf")
parser.add_argument("--stream",
    help="file containing the stream of tweets, one data point per line")
parser.add_argument("--outputfile",
    help="file to write the coreset to", default="coreset.txt")
parser.add_argument("--evalfile",
    help="file that contains the gold hashtag representations", type=str)
parser.add_argument("--delta",
    help="bound on the coefficients of the points",type=int)
parser.add_argument("--k",
    help="the number of clusters",type=int)
parser.add_argument("--memory",
    help="max number of entities that can be stored",type=int)
parser.add_argument("--dimensionality",
    help="dimension of features",type=int)
parser.add_argument("--coreset",
    help="build a coreset from the heavy representation",action="store_true")
parser.add_argument("--progress",
    help="print out progress",type=int)
args = parser.parse_args()

def process_line(line):
    result=line.replace(":false",":False")
    result=result.replace(":true",":True")
    result=result.replace(":null",":None")
    result=result.strip('\n')
    return result

def discretize(vec, D):
    # discretize an L1-normalized
    # vector with values in {1,..,D}
    # go from [-1,1] to [0,1]
    result=[(_r+1.)/2. for _r in vec]
    result=[int(round(_r*(D-1)))+1 for _r in result]
    return result

# create tfidf features on smaller set
tweets=[]
print('read tweets')
for line in open(args.train_stream,'r'): # set of tweets to learn vocab and idf from
    if '{' not in line:
        continue
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweets.append(full_thing["text"])
print('create tfidfvectorizer')
hasher = HashingVectorizer(n_features=args.dimensionality,norm='l1')
vectorizer = make_pipeline(hasher, TfidfTransformer())
print('fit data')
vectorizer.fit(tweets)
del tweets
truncation=(args.delta-1.)/2. # affects rounding of the features

# initialize index to integerize entities (either ent or hashtag)
ent_index=dict()
# ent representations
ent_reps=dict()

print("# stream tweets")
start=time.time()
counter=0
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" not in full_thing:
        continue # means it's not a tweet
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    tweet=full_thing["text"]
    ents=set(_h["text"] for _h in full_thing["entities"]["hashtags"])
    for ent in ents:
        ent_index.setdefault(ent,len(ent_index))
        enti=ent_index[ent]
        feats=vectorizer.transform([tweet]) # sparse vector
        discrete_feats=discretize([feats[0,ii] \
                for ii in range(args.dimensionality)],args.delta)
        ent_reps.setdefault(enti,([0.]*args.dimensionality,0))
        orep,numtweets=ent_reps[enti]
        nrep = [(_x*numtweets+_t)/(numtweets+1)\
                for (_x,_t) in zip(orep,discrete_feats)]
        ent_reps[enti] = (nrep, numtweets+1)
        if len(ent_reps)>args.memory:
            new_reps=dict()
            # cluster and make room
            # cluster
            s=set()
            for _,(_rep,_) in ent_reps.iteritems():
                s.add((tuple(_rep),1))
            kmedian=KMedian(args.k)
            seeds=range(10)
            kmedian.multiple_fits(s, 10, seeds)
            # subsample: get 10% of each cluster's points
            for ii in range(args.k):
                all_points=list(set(jj if a==ii else -1 \
                        for jj,a in enumerate(kmedian.assignments)))
                while -1 in all_points:
                    all_points.remove(-1)
                random.shuffle(all_points)
                subsample=all_points[:int(math.ceil(len(all_points)*0.1))]
                for jj in subsample:
                    # find corresponding entry in ent_reps
                    enti=None
                    for _enti,(_rep,_) in ent_reps.iteritems():
                        if _rep==kmedian.data[jj]:
                            enti=_enti
                            break
                    assert enti is not None
                    new_reps[enti]=ent_reps[enti]
            ent_reps=dict(new_reps)
            del new_reps
        # end if len(ent_reps)>args.memory

print("cluster")
s=set()
for _,(_rep,_) in ent_reps.iteritems():
    s.add((tuple(_rep),1))
kmedian=KMedian(args.k)
seeds=range(10)
cost=kmedian.multiple_fits(s, 10, seeds)
print("final clustering cost: " +str(cost))
print("clusters:")
for _m in kmedian.clusters:
    print(_m)
"""
# for each cluster, find most similar tweet
most_rep_vec=list([None]*len(kmedian.clusters))
most_rep_text=list([None]*len(kmedian.clusters))
counter=0
start=time.time()
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" not in full_thing:
        continue # means it's not a tweet
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    tweet=full_thing["text"]
    ents=set(_h["text"] for _h in full_thing["entities"]["hashtags"])
    feats=vectorizer.transform([tweet]) # sparse vector
    discrete_feats=[int(round(feats[0,ii]*truncation)+truncation+1)\
            for ii in range(args.dimensionality)]
    for ii,_t in enumerate(most_rep_vec):
        if _t is None:
            most_rep_vec[ii]=discrete_feats
            most_rep_text[ii]=tweet
        else:
            d1=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],_t))
            d2=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],discrete_feats))
            if d2<d1:
                most_rep_vec[ii]=discrete_feats
                most_rep_text[ii]=tweet
print("most representative tweets")
for ii,_t in enumerate(most_rep_text):
    print("===== cluster #"+str(ii+1) + "=====")
    print(_t)
    print("======================")
"""
clus2hash=[set() for _ in range(args.k)]
all_reps=dict()
assert args.evalfile is not None, "no eval file?"
print("load hashtag representations from file")
for line in open(args.evalfile,'r'):
    word,rep=line.strip('\n').split('\t')
    all_reps[word]=[float(_c) for _c in rep.split(' ')]
for ht,rep in all_reps.iteritems():
    distances=[sum((_a-_b)**2 for _a,_b in zip(rep,_q)) for _q in kmedian.clusters]
    ci=distances.index(min(distances))
    clus2hash[ci].add(ht.lower())
for ii,t in enumerate(clus2hash):
    clus2hash[ii]=list(t)
    print(t)
print("load word2vec")
model = gensim.models.KeyedVectors.load_word2vec_format(
    '/export/a12/nholzen/GoogleNews-vectors-negative300.bin',
    binary=True)
print("compute average similarities")
average_similarities=[0 for _ in range(args.k)]
for ii in range(args.k):
    print("cluster #" + str(ii+1))
    avg_sim=0
    counter=0
    for jj in range(len(clus2hash[ii])):
        if clus2hash[ii][jj] not in model.vocab:
            continue
        for kk in range(jj+1,len(clus2hash[ii])):
            if clus2hash[ii][kk] not in model.vocab:
                continue
            avg_sim+=model.similarity(clus2hash[ii][jj],\
                    clus2hash[ii][kk])
            counter+=1
    if counter>0:
        average_similarities[ii]=avg_sim/float(counter)
    else:
        average_similarities[ii]='empty cluster'
print("average similarities")
print(average_similarities)
