from kmedian_clustering import KMedian
import random

random.seed(24011993)

datafile="out.txt"

data=[]

for line in open(datafile,'r'):
    weight,point=line.strip('\n').split('\t')
    p=[float(_p) for _p in point.split(' ')]
    data.append((p,float(weight)))

seeds = [24011993*ii for ii in range(10)]
kmedian=KMedian(5)
cost=kmedian.multiple_fits(data, 10, seeds)
print(cost)
for _m in kmedian.clusters:
    print(_m)
