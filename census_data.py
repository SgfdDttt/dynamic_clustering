import argparse
import time
import random
import math
import ast
from kmedian_clustering import KMedian
from coreset import CoreSet

fixedseed=24011993
random.seed(fixedseed)

parser = argparse.ArgumentParser()
parser.add_argument("--stream",
    help="file containing the stream of tweets, one data point per line")
parser.add_argument("--outputfile",
    help="file to write the coreset to", default="coreset.txt")
parser.add_argument("--delta",
    help="bound on the coefficients of the points",type=int)
parser.add_argument("--k",
    help="the number of clusters",type=int)
parser.add_argument("--rho",
    help="probability of failure",type=float)
parser.add_argument("--epsilon",
    help="accuracy of algorithm",type=float)
parser.add_argument("--dimensionality",
    help="dimension of features",type=int)
parser.add_argument("--hashconstant",
    help="hash constant",default=None)
parser.add_argument("--progress",
    help="print out progress",type=int)
args = parser.parse_args()

print('initialize coreset')
cs_hc=float(args.hashconstant) if args.hashconstant is not None else None
cs=CoreSet(args.delta,args.dimensionality,args.k, \
        args.rho,args.epsilon,hash_constant=cs_hc)
print("stream points")
start=time.time()
counter=0
for line in open(args.stream,'r'):
    counter+=1
    if counter==1:
        continue
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time '+str(stop-start)+'s')
        start=time.time()
    point=[int(_c) for _c in line.strip('\n').split(',')[1:]]
    cs.process(1, point)

print("query coreset")
start=time.time()
s=cs.query(rectify_weights=True)
stop=time.time()
print("took " + str(stop-start) + "s")
if s=='FAIL':
    print('FAIL')
else:
    outfile=open(args.outputfile,'w')
    for x,y in s:
        outfile.write(str(y) + '\t' + ' '.join(str(c_) for c_ in x) + '\n')
    outfile.close()
    print("size of coreset: " + str(len(s)))

print("clustering")
kmedian=KMedian(args.k)
seeds=range(10)
cost=kmedian.multiple_fits(s, 10, seeds)
print("final clustering cost: " +str(cost))
for _m in kmedian.clusters:
    print(_m)
