import math
import random
import numpy as np

def euclidean_distance(p,q):
    assert len(p)==len(q), "len(p)="+str(len(p))+';len(q)='+str(len(q))
    return math.sqrt(sum((a-b)**2 for a,b in zip(p,q)))

def sign(x):
    if x==0:
        return 0
    if x>0:
        return 1
    if x<0:
        return -1

class KMedian():
    def __init__(self,num_clusters):
        self.k=num_clusters
        self.clusters=[]

    def assign_clusters(self):
        self.assignments=[]
        cost=0
        for (_p,_w) in zip(self.data, self.weights):
            distances = [euclidean_distance(_p,_q) for _q in self.clusters]
            self.assignments.append(distances.index(min(distances)))
            cost+=min(distances)*_w
        return cost

    def compute_medians(self):
        self.clusters=[]
        for ii in range(self.k): # index of cluster
            points=[]
            for jj,_a in enumerate(self.assignments):
                if _a==ii:
                    for _ in range(int(self.weights[jj])):
                        points.append(self.data[jj])
            if len(points)>0:
                medians=np.median(np.array(points),axis=0).tolist()
                self.clusters.append(medians)
            else:
                self.clusters.append([0]*len(self.data[0]))
        assert len(self.clusters)==self.k

    def fit(self, num_iterations):
        self.clusters=[]
        self.assignments=[]
        # randomly assign clusters
        for _ in range(len(self.data)):
            self.assignments.append(int(random.random()*self.k))
        for it in range(num_iterations):
            self.compute_medians()
            cost=self.assign_clusters()
        return cost

    def multiple_fits(self, data, num_iterations, seeds):
        # data is a list of pairs (point, weight)
        self.data,self.weights=[],[]
        for _p,_w in data:
            if _w>0:
                self.data.append(list(_p))
                self.weights.append(_w)
        best_clusters=[]
        best_cost=None
        for s in seeds:
            random.seed(s)
            cost=self.fit(num_iterations)
            if best_cost is None:
                best_cost=cost
                best_clusters=list(self.clusters)
            elif cost<best_cost: 
                best_cost=cost
                best_clusters=list(self.clusters)
        self.clusters=best_clusters
        self.assign_clusters() # get up to date cluster assignments
        return best_cost
