import random
import math

"""
class TestSingleton():
    # following the notation of Ganguly 2007
    # this class is separate from the KSet class
    # for the purpose of clarity of the code
    def __init__(self):
        self.m=0
        self.U=0
        self.V=0

    def tsupdate(self,i,v):
        assert isinstance(i,int), "i="+str(i)
        self.m+=v
        self.U+=v*i
        self.V+=v*i*i

    def tscard(self):
        if self.m==0:
            #return "EMPTY"
            return False,(0,0)
        elif self.U**2 == self.m * self.V:
            #return ("SINGLETON",float(self.U)/self.m,self.m)
            # the test above should guarantee that U/m is an integer
            return True,(self.U/self.m,self.m)
        else:
            #return "COLLISION"
            return False,(0,0)

    def __str__(self):
        return ','.join([str(self.m),str(self.U),str(self.V)])
"""

class KSet():
    # following the notation of Ganguly 2007
    def __init__(self,k,delta):
        assert isinstance(k, int), "k="+str(k)
        assert (delta > 0), "delta="+str(delta)
        self.R = int(math.ceil(math.log(k/delta)))
        self.B = 2*k
        self.m = 0
        # set up table H
        self.H = dict()
        # set up hash functions (buckets)
        self.hashes=dict()

    def ksetupdate(self,i,v):
        assert isinstance(i,int), "i="+str(i)
        self.m+=v
        for rr in range(self.R):
            self.hashes.setdefault((rr,i), int(random.random()*self.B))
            key=(rr,self.hashes[(rr,i)])
            self.H.setdefault(key,(0,0,0))
            old=self.H[key]
            self.H[key]=(old[0]+v,old[1]+i*v,old[2]+i*i*v)

    def returnset(self):
        S=set() # because there may be duplicates
        for key in self.H:
            # if some element is not in self.H it means issingleton is
            # false for that element anyway
            counts=self.H[key]
            #issingleton,(x,y)=self.H[key].tscard()
            # x is the item, y its frequency
            if counts[0]!=0:
                if counts[1]**2 == counts[0]*counts[2]:
                    S.add((counts[1]/counts[0],counts[0]))
        S=list(S)
        if self.m==sum(_y for (_,_y) in S):
            return S
        else:
            return "NIL"
