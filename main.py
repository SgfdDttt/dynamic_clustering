import argparse
import time
import random
import math
import ast
from kmedian_clustering import KMedian
from coreset import CoreSet
from heavy_hitter import HeavyHitter
from sklearn.feature_extraction.text import TfidfVectorizer,TfidfTransformer,HashingVectorizer
from sklearn.pipeline import make_pipeline

fixedseed=24011993
random.seed(fixedseed)

parser = argparse.ArgumentParser()
parser.add_argument("--train_stream",
    help="file containing the stream of tweets to use to set up vocabulary and tfidf")
parser.add_argument("--stream",
    help="file containing the stream of tweets, one data point per line")
parser.add_argument("--outputfile",
    help="file to write the coreset to", default="coreset.txt")
parser.add_argument("--delta",
    help="bound on the coefficients of the points",type=int)
parser.add_argument("--k",
    help="the number of clusters",type=int)
parser.add_argument("--rho",
    help="probability of failure",type=float)
parser.add_argument("--epsilon",
    help="accuracy of algorithm",type=float)
parser.add_argument("--dimensionality",
    help="dimension of features",type=int)
parser.add_argument("--numents",
    help="estimate on the number of ents",type=int)
parser.add_argument("--coreset",
    help="build a coreset from the heavy representation",action="store_true")
parser.add_argument("--hashconstant",
    help="hash constant",default=None)
parser.add_argument("--memory",
    help="build a coreset from the heavy representation",action="store_true")
parser.add_argument("--progress",
    help="print out progress",type=int)
args = parser.parse_args()

def process_line(line):
    result=line.replace(":false",":False")
    result=result.replace(":true",":True")
    result=result.replace(":null",":None")
    result=result.strip('\n')
    return result

# create tfidf features on smaller set
tweets=[]
print('read tweets for IDF features, start timer')
start=time.time()
for line in open(args.train_stream,'r'): # set of tweets to learn vocab and idf from
    if '{' not in line:
        continue
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweets.append(full_thing["text"])
print('create tfidfvectorizer')
hasher = HashingVectorizer(n_features=args.dimensionality,norm='l1')
vectorizer = make_pipeline(hasher, TfidfTransformer())
print('fit data')
vectorizer.fit(tweets)
stop=time.time()
print('took ' + str(stop-start) + 's')
del tweets
truncation=(args.delta-1.)/2. # affects rounding of the features

# initialize index to integerize entities (either ent or hashtag)
ent_index=dict()
# heavy-hitter to track ents
hh_epsilon=0.1
hh_k=int(math.ceil(1000./(hh_epsilon**2)))
print("hh_k")
print(hh_k)
r=0.1
heavy_ents=HeavyHitter(args.numents,hh_k,hh_epsilon,args.rho)
light_ents=HeavyHitter(args.numents,int(math.floor(hh_k/r)),r*hh_epsilon,args.rho)
# ent representations
ent_reps=dict()

print("stream tweets")
start=time.time()
counter=0
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" not in full_thing:
        continue # means it's not a tweet
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    tweet=full_thing["text"]
    ents=set(_h["text"] for _h in full_thing["entities"]["hashtags"])
    for ent in ents:
        # check if ent is heavy
        ent_index.setdefault(ent,len(ent_index))
        enti=ent_index[ent]
        heavy_ents.process(enti)
        light_ents.process(enti)
        light = enti in set(_x for _x,_ in light_ents.top_k)
        if light: # then update the representation
            feats=vectorizer.transform([tweet]) # sparse vector
            discrete_feats=[int(round(feats[0,ii]*truncation)+truncation+1)\
                    for ii in range(args.dimensionality)]
            ent_reps.setdefault(enti,([0.]*args.dimensionality,0))
            orep,numtweets=ent_reps[enti]
            nrep = [(_x*numtweets+_t)/(numtweets+1)\
                    for (_x,_t) in zip(orep,discrete_feats)]
            ent_reps[enti] = (nrep, numtweets+1)
        else: # else delete the representation if it exists
            ent_reps.pop(enti,None)

# produce coreset and write to file
if args.coreset:
    # initialize coreset
    cs_hc=None
    if args.hashconstant is not None:
        cs_hc=float(args.hashconstant)
    cs=CoreSet(args.delta,args.dimensionality,args.k, \
            args.rho,args.epsilon,hash_constant=cs_hc)
    print("produce coreset")
else:
    s=set()
for _hi in set(_x for _x,_y in heavy_ents.top_k):
    if _hi in ent_reps:
        # strangely that's not always the
        # case -- should be marginal in practice
        _rep,_y=ent_reps[_hi]
        if args.coreset:
            cs.process(1, [int(round(_r)) for _r in _rep])
        else:
            s.add((tuple([int(round(_r)) for _r in _rep]),_y))
if args.coreset:
    print("query coreset")
    start=time.time()
    s=cs.query(rectify_weights=True)
    stop=time.time()
    print("took " + str(stop-start) + "s")
if s=='FAIL':
    print('FAIL')
else:
    outfile=open(args.outputfile,'w')
    for x,y in s:
        outfile.write(str(y) + '\t' + ' '.join(str(c_) for c_ in x) + '\n')
    outfile.close()
    print("size of coreset: " + str(len(s)))

print("clustering")
kmedian=KMedian(args.k)
seeds=range(10)
cost=kmedian.multiple_fits(s, 10, seeds)
print("final clustering cost: " +str(cost))
for _m in kmedian.clusters:
    print(_m)
# for each cluster, find most similar tweet
most_rep_vec=list([None]*len(kmedian.clusters))
most_rep_text=list([None]*len(kmedian.clusters))
counter=0
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" not in full_thing:
        continue # means it's not a tweet
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    tweet=full_thing["text"]
    ents=set(_h["text"] for _h in full_thing["entities"]["hashtags"])
    feats=vectorizer.transform([tweet]) # sparse vector
    discrete_feats=[int(round(feats[0,ii]*truncation)+truncation+1)\
            for ii in range(args.dimensionality)]
    for ii,_t in enumerate(most_rep_vec):
        if _t is None:
            most_rep_vec[ii]=discrete_feats
            most_rep_text[ii]=tweet
        else:
            d1=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],_t))
            d2=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],discrete_feats))
            if d2<d1:
                most_rep_vec[ii]=discrete_feats
                most_rep_text[ii]=tweet
print("most representative tweets")
for ii,_t in enumerate(most_rep_text):
    print("===== cluster #"+str(ii+1) + "=====")
    print(_t)
    print("======================")
