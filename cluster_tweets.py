import argparse
import time
import random
import math
import ast
from kmedian_clustering import KMedian
from coreset import CoreSet
from sklearn.feature_extraction.text import TfidfVectorizer,TfidfTransformer,HashingVectorizer
from sklearn.pipeline import make_pipeline

fixedseed=24011993
random.seed(fixedseed)

parser = argparse.ArgumentParser()
parser.add_argument("--train_stream",
    help="file containing the stream of tweets to use to set up vocabulary and tfidf")
parser.add_argument("--stream",
    help="file containing the stream of tweets, one data point per line")
parser.add_argument("--outputfile",
    help="file to write the coreset to", default="coreset.txt")
parser.add_argument("--delta",
    help="bound on the coefficients of the points",type=int)
parser.add_argument("--k",
    help="the number of clusters",type=int)
parser.add_argument("--rho",
    help="probability of failure",type=float)
parser.add_argument("--epsilon",
    help="accuracy of algorithm",type=float)
parser.add_argument("--dimensionality",
    help="dimension of features",type=int)
parser.add_argument("--hashconstant",
    help="hash constant",default=None)
parser.add_argument("--progress",
    help="print out progress",type=int)
parser.add_argument("--nounicode",
    help="ignore unicode",action="store_true")
args = parser.parse_args()

def process_line(line):
    result=line.replace(":false",":False")
    result=result.replace(":true",":True")
    result=result.replace(":null",":None")
    result=result.strip('\n')
    return result

# create tfidf features on smaller set
tweets=[]
print('read tweets for IDF features, start timer')
start=time.time()
for line in open(args.train_stream,'r'): # set of tweets to learn vocab and idf from
    if '{' not in line:
        continue
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweet=full_thing["text"]
    elif "body" in full_thing:
        tweet=full_thing["body"]
    else:
        continue
    if (args.nounicode) and ("\u" in tweet):
        continue
    tweets.append(tweet)
print('create tfidfvectorizer')
hasher = HashingVectorizer(n_features=args.dimensionality,norm='l1')
vectorizer = make_pipeline(hasher, TfidfTransformer())
print('fit data')
vectorizer.fit(tweets)
stop=time.time()
print('took ' + str(stop-start) + 's')
del tweets
truncation=(args.delta-1.)/2. # affects rounding of the features

# initialize coreset
cs_hc=None
if args.hashconstant is not None:
    cs_hc=float(args.hashconstant)
cs=CoreSet(args.delta,args.dimensionality,args.k, \
        args.rho,args.epsilon,hash_constant=cs_hc)
print("stream tweets")
start=time.time()
counter=0
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweet=full_thing["text"]
    elif "body" in full_thing:
        tweet=full_thing["body"]
    else:
        continue
    if (args.nounicode) and ("\u" in tweet):
        continue
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    feats=vectorizer.transform([tweet]) # sparse vector
    discrete_feats=[int(round(feats[0,ii]*truncation)+truncation+1)\
            for ii in range(args.dimensionality)]
    cs.process(1, discrete_feats)

# produce coreset and write to file
print("query coreset")
start=time.time()
s=cs.query(rectify_weights=True)
stop=time.time()
print("took " + str(stop-start) + "s")
if s=='FAIL':
    print('FAIL')
else:
    outfile=open(args.outputfile,'w')
    for x,y in s:
        outfile.write(str(y) + '\t' + ' '.join(str(c_) for c_ in x) + '\n')
    outfile.close()
    print("size of coreset: " + str(len(s)))

print("clustering")
kmedian=KMedian(args.k)
seeds=range(10)
cost=kmedian.multiple_fits(s, 10, seeds)
print("final clustering cost: " +str(cost))
for _m in kmedian.clusters:
    print(_m)
# for each cluster, find most similar tweet
most_rep_vec=list([None]*len(kmedian.clusters))
most_rep_text=list([None]*len(kmedian.clusters))
counter=0
start=time.time()
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweet=full_thing["text"]
    elif "body" in full_thing:
        tweet=full_thing["body"]
    else:
        continue
    if (args.nounicode) and ("\u" in tweet):
        continue
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    feats=vectorizer.transform([tweet]) # sparse vector
    discrete_feats=[int(round(feats[0,ii]*truncation)+truncation+1)\
            for ii in range(args.dimensionality)]
    for ii,_t in enumerate(most_rep_vec):
        if _t is None:
            most_rep_vec[ii]=discrete_feats
            most_rep_text[ii]=tweet
        else:
            d1=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],_t))
            d2=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],discrete_feats))
            if d2<d1:
                most_rep_vec[ii]=discrete_feats
                most_rep_text[ii]=tweet
print("most representative tweets")
for ii,_t in enumerate(most_rep_text):
    print("===== cluster #"+str(ii+1) + "=====")
    print(_t)
    print("======================")
