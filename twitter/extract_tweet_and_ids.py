import sys

first_line=True
for line in open(sys.argv[1],'r'):
    if len(line.strip('\n'))==0:
        continue
    if first_line:
        first_line=False
        continue
    if line[2:8]=="delete":
        continue # ignore deletions
    text=line.split('\"text\":')[1].split(',\"source\":')[0].strip('"')
    tweet_id=line.split('\"id\":')[1].split(',\"id_str\":')[0].strip('"')
    user_id_tmp=line.split('\"user\":')[1].split(',\"name\":')[0]
    user_id=user_id_tmp.split('\"id\":')[1].split(',\"id_str\":')[0].strip('"')
    print(text + '\t' + user_id + '\t' + tweet_id)
