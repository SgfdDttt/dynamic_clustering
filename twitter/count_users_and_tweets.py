import sys

users,tweets=set(),set()
for line in open(sys.argv[1], 'r'):
    _,user,tweetid=line.strip('\n').split('\t')
    users.add(user)
    tweets.add(tweetid)
print('# of distinct users: ' + str(len(users)))
print('# of distinct tweet ids: ' + str(len(tweets)))
