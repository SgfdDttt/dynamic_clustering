import argparse
import gensim
import time
import random
import math
import ast
from kmedian_clustering import KMedian
from coreset import CoreSet
from heavy_hitter import HeavyHitter
from sklearn.feature_extraction.text import TfidfVectorizer,TfidfTransformer,HashingVectorizer
from sklearn.pipeline import make_pipeline

fixedseed=24011993
random.seed(fixedseed)

parser = argparse.ArgumentParser()
parser.add_argument("--train_stream",
    help="file containing the stream of tweets to use to set up vocabulary and tfidf")
parser.add_argument("--stream",
    help="file containing the stream of tweets, one data point per line")
parser.add_argument("--evalfile",
    help="file that contains the gold hashtag representations", type=str)
parser.add_argument("--outputfile",
    help="file to write the coreset to", default="coreset.txt")
parser.add_argument("--delta",
    help="bound on the coefficients of the points",type=int)
parser.add_argument("--k",
    help="the number of clusters",type=int)
parser.add_argument("--rho",
    help="probability of failure",type=float)
parser.add_argument("--epsilon",
    help="accuracy of algorithm",type=float)
parser.add_argument("--dimensionality",
    help="dimension of features",type=int)
parser.add_argument("--memory",
    help="number of items kept by the heavy hitter",type=int)
parser.add_argument("--progress",
    help="print out progress",type=int)
args = parser.parse_args()

def process_line(line):
    result=line.replace(":false",":False")
    result=result.replace(":true",":True")
    result=result.replace(":null",":None")
    result=result.strip('\n')
    return result

def discretize(vec, D):
    # discretize an L1-normalized
    # vector with values in {1,..,D}
    # go from [-1,1] to [0,1]
    result=[(_r+1.)/2. for _r in vec]
    result=[int(round(_r*(D-1)))+1 for _r in result]
    return result

# create tfidf features on smaller set
tweets=[]
print('read tweets for IDF features, start timer')
start=time.time()
for line in open(args.train_stream,'r'): # set of tweets to learn vocab and idf from
    if '{' not in line:
        continue
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweets.append(full_thing["text"])
print('create tfidfvectorizer')
hasher = HashingVectorizer(n_features=args.dimensionality,norm='l1')
vectorizer = make_pipeline(hasher, TfidfTransformer())
print('fit data')
vectorizer.fit(tweets)
stop=time.time()
print('took ' + str(stop-start) + 's')
del tweets
truncation=(args.delta-1.)/2. # affects rounding of the features

# initialize index to integerize entities (either ent or hashtag)
ent_index=dict()
# heavy-hitter to track ents
hh_epsilon=args.epsilon
hh_k=args.memory #int(math.ceil(1000./(hh_epsilon**2)))
print("hh_k")
print(hh_k)
r=0.5
hh_n=args.delta**args.dimensionality
heavy_ents=HeavyHitter(hh_n,hh_k,hh_epsilon,args.rho)
light_ents=HeavyHitter(hh_n,int(math.floor(hh_k/r)),r*hh_epsilon,args.rho)
# ent representations
ent_reps=dict()

print("stream tweets")
start=time.time()
counter=0
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" not in full_thing:
        continue # means it's not a tweet
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    tweet=full_thing["text"]
    ents=set(_h["text"] for _h in full_thing["entities"]["hashtags"])
    for ent in ents:
        # check if ent is heavy
        ent_index.setdefault(ent,len(ent_index))
        enti=ent_index[ent]
        heavy_ents.process(enti)
        light_ents.process(enti)
        light = enti in set(_x for _x,_ in light_ents.top_k)
        if light: # then update the representation
            feats=vectorizer.transform([tweet]) # sparse vector
            discrete_feats=discretize([feats[0,ii] \
                    for ii in range(args.dimensionality)],args.delta)
            ent_reps.setdefault(enti,([0.]*args.dimensionality,0))
            orep,numtweets=ent_reps[enti]
            nrep = [(_x*numtweets+_t)/(numtweets+1)\
                    for (_x,_t) in zip(orep,discrete_feats)]
            ent_reps[enti] = (nrep, numtweets+1)
        else: # else delete the representation if it exists
            ent_reps.pop(enti,None)

# produce coreset and write to file
s=set()
for _hi in set(_x for _x,_y in heavy_ents.top_k):
    if _hi in ent_reps:
        # strangely that's not always the
        # case -- should be marginal in practice
        _rep,_y=ent_reps[_hi]
        s.add((tuple([int(round(_r)) for _r in _rep]),1))
if s=='FAIL':
    print('FAIL')
else:
    outfile=open(args.outputfile,'w')
    for x,y in s:
        outfile.write(str(y) + '\t' + ' '.join(str(c_) for c_ in x) + '\n')
    outfile.close()
    print("size of coreset: " + str(len(s)))

print("clustering")
kmedian=KMedian(args.k)
seeds=range(10)
cost=kmedian.multiple_fits(s, 10, seeds)
print("final clustering cost: " +str(cost))
for _m in kmedian.clusters:
    print(_m)
"""
# for each cluster, find most similar tweet
most_rep_vec=list([None]*len(kmedian.clusters))
most_rep_text=list([None]*len(kmedian.clusters))
counter=0
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" not in full_thing:
        continue # means it's not a tweet
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    tweet=full_thing["text"]
    ents=set(_h["text"] for _h in full_thing["entities"]["hashtags"])
    feats=vectorizer.transform([tweet]) # sparse vector
    discrete_feats=[int(round(feats[0,ii]*truncation)+truncation+1)\
            for ii in range(args.dimensionality)]
    for ii,_t in enumerate(most_rep_vec):
        if _t is None:
            most_rep_vec[ii]=discrete_feats
            most_rep_text[ii]=tweet
        else:
            d1=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],_t))
            d2=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],discrete_feats))
            if d2<d1:
                most_rep_vec[ii]=discrete_feats
                most_rep_text[ii]=tweet
print("most representative tweets")
for ii,_t in enumerate(most_rep_text):
    print("===== cluster #"+str(ii+1) + "=====")
    print(_t)
    print("======================")
"""
clus2hash=[set() for _ in range(args.k)]
all_reps=dict()
assert args.evalfile is not None, "no eval file?"
print("load hashtag representations from file")
for line in open(args.evalfile,'r'):
    word,rep=line.strip('\n').split('\t')
    all_reps[word]=[float(_c) for _c in rep.split(' ')]
for ht,rep in all_reps.iteritems():
    distances=[sum((_a-_b)**2 for _a,_b in zip(rep,_q)) for _q in kmedian.clusters]
    ci=distances.index(min(distances))
    clus2hash[ci].add(ht.lower())
for ii,t in enumerate(clus2hash):
    clus2hash[ii]=list(t)
    print(t)
print("load word2vec")
model = gensim.models.KeyedVectors.load_word2vec_format(
    '/export/a12/nholzen/GoogleNews-vectors-negative300.bin',
    binary=True)
print("compute average similarities")
average_similarities=[0 for _ in range(args.k)]
for ii in range(args.k):
    print("cluster #" + str(ii+1))
    avg_sim=0
    counter=0
    for jj in range(len(clus2hash[ii])):
        if clus2hash[ii][jj] not in model.vocab:
            continue
        for kk in range(jj+1,len(clus2hash[ii])):
            if clus2hash[ii][kk] not in model.vocab:
                continue
            avg_sim+=model.similarity(clus2hash[ii][jj],clus2hash[ii][kk])
            counter+=1
    if counter>0:
        average_similarities[ii]=avg_sim/float(counter)
    else:
        average_similarities[ii]='empty cluster'
print("average similarities")
print(average_similarities)
