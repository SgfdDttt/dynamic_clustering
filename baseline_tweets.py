import argparse
import time
import random
import math
import ast
from kmedian_clustering import KMedian
from sklearn.feature_extraction.text import TfidfVectorizer,TfidfTransformer,HashingVectorizer
from sklearn.pipeline import make_pipeline

fixedseed=24011993
random.seed(fixedseed)

parser = argparse.ArgumentParser()
parser.add_argument("--train_stream",
    help="file containing the stream of tweets to use to set up vocabulary and tfidf")
parser.add_argument("--stream",
    help="file containing the stream of tweets, one data point per line")
parser.add_argument("--outputfile",
    help="file to write the coreset to", default="coreset.txt")
parser.add_argument("--delta",
    help="bound on the coefficients of the points",type=int)
parser.add_argument("--k",
    help="the number of clusters",type=int)
parser.add_argument("--memory",
    help="max number of entities that can be stored",type=int)
parser.add_argument("--dimensionality",
    help="dimension of features",type=int)
parser.add_argument("--progress",
    help="print out progress",type=int)
parser.add_argument("--nounicode",
    help="ignore unicode",action="store_true")
args = parser.parse_args()

def process_line(line):
    result=line.replace(":false",":False")
    result=result.replace(":true",":True")
    result=result.replace(":null",":None")
    result=result.strip('\n')
    return result

def discretize(vec, D):
    # discretize an L1-normalized
    # vector with values in {1,..,D}
    # go from [-1,1] to [0,1]
    result=[(_r+1.)/2. for _r in vec]
    result=[int(round(_r*(D-1)))+1 for _r in result]
    assert isinstance(result,list)
    return result

# create tfidf features on smaller set
tweets=[]
print('read tweets')
for line in open(args.train_stream,'r'): # set of tweets to learn vocab and idf from
    if '{' not in line:
        continue
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweet=full_thing["text"]
    elif "body" in full_thing:
        tweet=full_thing["body"]
    else:
        continue
    if (args.nounicode) and ("\u" in tweet):
        continue
    tweets.append(tweet)
print('create tfidfvectorizer')
hasher = HashingVectorizer(n_features=args.dimensionality,norm='l1')
vectorizer = make_pipeline(hasher, TfidfTransformer())
print('fit data')
vectorizer.fit(tweets)
del tweets
truncation=(args.delta-1.)/2. # affects rounding of the features

print("# stream tweets")
start=time.time()
counter=0
points=[]
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweet=full_thing["text"]
    elif "body" in full_thing:
        tweet=full_thing["body"]
    else:
        continue
    if (args.nounicode) and ("\u" in tweet):
        continue
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    feats=vectorizer.transform([tweet]) # sparse vector
    discrete_feats=discretize([feats[0,ii] \
            for ii in range(args.dimensionality)],args.delta)
    assert isinstance(discrete_feats,list)
    points.append(discrete_feats)
    if len(points)>args.memory:
        new_points=[]
        # cluster and make room
        # cluster
        s=set()
        #print(points)
        for _rep in points:
            assert isinstance(_rep,list), "_rep="+str(_rep)
            #print(type((tuple(_rep),1)))
            s.add((tuple(_rep),1))
        kmedian=KMedian(args.k)
        seeds=range(10)
        kmedian.multiple_fits(s, 10, seeds)
        # subsample: get 10% of each cluster's points
        for ii in range(args.k):
            all_points=list(set(jj if a==ii else -1 \
                    for jj,a in enumerate(kmedian.assignments)))
            while -1 in all_points:
                all_points.remove(-1)
            random.shuffle(all_points)
            subsample=all_points[:int(math.ceil(len(all_points)*0.1))]
        for jj in subsample:
            new_points.append(kmedian.data[jj])
        points=list(new_points)
        del new_points
    # end if len(points)>args.memory

print("cluster")
s=set()
for _rep in points:
    s.add((tuple(_rep),1))
kmedian=KMedian(args.k)
seeds=range(10)
cost=kmedian.multiple_fits(s, 10, seeds)
print("# of points: " + str(len(points)))
print("final clustering cost: " +str(cost))
print("clusters:")
for _m in kmedian.clusters:
    print(_m)
# for each cluster, find most similar tweet
most_rep_vec=list([None]*len(kmedian.clusters))
most_rep_text=list([None]*len(kmedian.clusters))
counter=0
start=time.time()
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweet=full_thing["text"]
    elif "body" in full_thing:
        tweet=full_thing["body"]
    else:
        continue
    if (args.nounicode) and ("\u" in tweet):
        continue
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    feats=vectorizer.transform([tweet]) # sparse vector
    discrete_feats=discretize([feats[0,ii] \
            for ii in range(args.dimensionality)],args.delta)
    for ii,_t in enumerate(most_rep_vec):
        if _t is None:
            most_rep_vec[ii]=discrete_feats
            most_rep_text[ii]=tweet
        else:
            d1=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],_t))
            d2=sum((_a-_b)**2 for _a,_b in zip(kmedian.clusters[ii],discrete_feats))
            if d2<d1:
                most_rep_vec[ii]=discrete_feats
                most_rep_text[ii]=tweet
print("most representative tweets")
for ii,_t in enumerate(most_rep_text):
    print("===== cluster #"+str(ii+1) + "=====")
    print(_t)
    print("======================")
