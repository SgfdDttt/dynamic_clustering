import argparse
import time
import ast
from sklearn.feature_extraction.text import TfidfVectorizer,TfidfTransformer,HashingVectorizer
from sklearn.pipeline import make_pipeline

parser = argparse.ArgumentParser()
parser.add_argument("--train_stream",
    help="file containing the stream of tweets to use to set up vocabulary and tfidf")
parser.add_argument("--stream",
    help="file containing the stream of tweets, one data point per line")
parser.add_argument("--outputfile",
    help="file to write the coreset to", default="coreset.txt")
parser.add_argument("--delta",
    help="bound on the coefficients of the points",type=int)
parser.add_argument("--dimensionality",
    help="dimension of features",type=int)
parser.add_argument("--progress",
    help="print out progress",type=int)
args = parser.parse_args()

def process_line(line):
    result=line.replace(":false",":False")
    result=result.replace(":true",":True")
    result=result.replace(":null",":None")
    result=result.strip('\n')
    return result

def discretize(vec, D):
    # discretize an L1-normalized
    # vector with values in {1,..,D}
    # go from [-1,1] to [0,1]
    result=[(_r+1.)/2. for _r in vec]
    result=[int(round(_r*(D-1)))+1 for _r in result]
    return result

# create tfidf features on smaller set
tweets=[]
print('read tweets for IDF features, start timer')
start=time.time()
for line in open(args.train_stream,'r'): # set of tweets to learn vocab and idf from
    if '{' not in line:
        continue
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweets.append(full_thing["text"])
print('create tfidfvectorizer')
hasher = HashingVectorizer(n_features=args.dimensionality,norm='l1')
vectorizer = make_pipeline(hasher, TfidfTransformer())
print('fit data')
vectorizer.fit(tweets)
stop=time.time()
print('took ' + str(stop-start) + 's')
del tweets

# ent representations
ent_reps=dict()

print("stream tweets")
start=time.time()
counter=0
for line in open(args.stream,'r'):
    if '{' not in line:
        continue
    counter+=1
    full_thing = ast.literal_eval(process_line(line))
    if "text" not in full_thing:
        continue # means it's not a tweet
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    tweet=full_thing["text"]
    ents=set(_h["text"] for _h in full_thing["entities"]["hashtags"])
    for ent in ents:
        # check if ent is heavy
        ent_reps.setdefault(ent, ([0]*args.dimensionality,0))
        feats=vectorizer.transform([tweet]) # sparse vector
        discrete_feats=discretize([feats[0,ii] \
                for ii in range(args.dimensionality)],args.delta)
        orep,numtweets=ent_reps[ent]
        nrep = [(_x*numtweets+_t)/(numtweets+1)\
                for (_x,_t) in zip(orep,discrete_feats)]
        ent_reps[ent] = (nrep, numtweets+1)

# produce coreset and write to file
print("write to file")
outfile=open(args.outputfile,'w')
for ht, (rep,_) in ent_reps.iteritems():
    outfile.write(str(ht) + '\t' + ' '.join(str(c_) for c_ in rep) + '\n')
outfile.close()
