import math
import numpy as np
from sklearn.mixture import GaussianMixture

fixedseed=24011993
epsilon=1e-8
num_points=65536
num_components=10
d=2
Delta=512

gmm=GaussianMixture(num_components,covariance_type='full')
np.random.seed(fixedseed)
# weights for each component
priors=Delta*np.random.random((num_components,))
priors=np.exp(priors-np.max(priors))
priors=priors/np.sum(priors)
gmm.weights_=priors
# mean for each component
gmm.means_=Delta*np.random.random((num_components,d))
# covariance for each component
gmm.precisions_,gmm.covariances_,gmm.precisions_cholesky_=[],[],[]
for ii in range(num_components):
    covar=(Delta/8)*np.random.random((d, d))
    covar=np.dot(covar,np.transpose(covar,(1,0)))
    gmm.covariances_.append(covar)
    gmm.precisions_.append(np.linalg.inv(gmm.covariances_[-1]))
    gmm.precisions_cholesky_.append(np.linalg.cholesky(gmm.precisions_[-1]))
gmm.precisions_=np.array(gmm.precisions_)
gmm.precisions_cholesky_=np.array(gmm.precisions_cholesky_)
# pretend it has converged
gmm.converged_=True
# sample
points,labels=gmm.sample(num_points)
# round points
stream=[]
for p_ in points:
    new_p=[max(min(Delta,int(s_)),0) for s_ in p_]
    print(' '.join(str(p_) for p_ in new_p))
