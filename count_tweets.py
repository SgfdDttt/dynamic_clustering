import ast
import sys

def process_line(line):
    result=line.replace(":false",":False")
    result=result.replace(":true",":True")
    result=result.replace(":null",":None")
    result=result.strip('\n')
    return result

counter=0
for line in open(sys.argv[1],'r'): # set of tweets to learn vocab and idf from
    if '{' not in line:
        continue
    full_thing = ast.literal_eval(process_line(line))
    if "text" in full_thing:
        tweet=full_thing["text"]
    elif "body" in full_thing:
        tweet=full_thing["body"]
    else:
        continue
    if "\\u" in tweet:
        continue
    counter+=1
print("number of tweets: " + str(counter))
