import math
import random

def median(l):
    r,q=(len(l)-1)%2,(len(l)-1)/2
    l2=sorted(l)
    m=(l2[q]+l2[q+r])/2.
    return int(m)

class HeavyHitter():
    # following notation of Charikar et al.
    def __init__(self,n,k,epsilon,delta):
        assert isinstance(n, int) or isinstance(n, long), "n="+str(n)
        assert isinstance(k, int), "k="+str(k)
        assert (delta > 0), "delta="+str(delta) # not sure about this one
        assert (epsilon > 0), "epsilon="+str(epsilon) # not sure about this one
        self.k=k
        # TODO fix b
        # t = O(log (n/delta)) Charikar Section 4 par 2, details in journal
        constant_factor=(1+4*math.log(4)-4*math.log(5))/8
        self.t=int(math.ceil(constant_factor*math.log(float(n)/delta)))
        # b \geq max(8k, 32/(epsilon^2) "not exactly") # Charikar Lemma 5
        self.b=int(math.ceil(max(8.*k, 32./(epsilon**2))))
        self.top_k=[]
        self.table=dict()
        # set up buckets, initialize on the fly
        # t functions, from objects to {1,...,b}
        self.buckets=dict()
        # set up hashes, initialize on the fly
        # t hash functions from objects to {+1,-1}
        self.hashes=dict()

    def add(self, q, add=1): # add argument can be -1 to remove
        assert isinstance(q,int) or isinstance(q,long), "q="+str(q)
        for ii in range(self.t):
            self.buckets.setdefault((ii,q), int(random.random()*self.b))
            self.hashes.setdefault((ii,q),int(random.random()*2)*2-1)
            bucket=self.buckets[(ii,q)]
            self.table.setdefault((ii,bucket),0)
            self.table[(ii,bucket)]+=add*self.hashes[(ii,q)]

    def estimate(self, q):
        assert isinstance(q,int), "q="+str(q)
        estimates=[]
        for ii in range(self.t):
            if (ii,q) not in self.buckets or\
                    (ii,q) not in self.hashes: # then this was never added to the table
                estimates.append(0)
            else:
                bucket=self.buckets[(ii,q)]
                estimates.append(self.hashes[(ii,q)]*self.table.get((ii,bucket),0))
        return median(estimates)

    def insert_into_sorted(self, item, count):
        if len(self.top_k)==0:
            self.top_k.append((item,count))
            return
        # if item already in heap, update value
        for ii,(it,_) in enumerate(self.top_k):
            if it==item:
                self.top_k.pop(ii)
                self.insert_into_sorted(item,count)
                return
        # if item not in heap, insert at correct spot
        index=len(self.top_k)-1
        while True: # start at bottom, go up to top of heap
            if self.top_k[index][1] > count: # means it should be inserted here
                break
            index-=1
            if index==-1: # reached top of heap
                break
        self.top_k.insert(index+1,(item,count))
        self.top_k=self.top_k[:self.k] # keep at most k items

    def process(self, q, op=1):
        self.add(q,add=op)
        e = self.estimate(q)
        self.insert_into_sorted(q,e)
