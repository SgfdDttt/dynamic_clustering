import math
import random
from heavy_hitter import HeavyHitter
from k_set import KSet
import itertools

def memoize(function):
    memo = {}
    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv
    return wrapper

def log(x): # log base 2
    return math.log(x,2)

def find_rho(big_delta,d,k,epsilon,space_constraint):
    # find appropriate value for failure probability given  
    # space constraints by doing a dichotomic search
    #failure=compute_space(big_delta,d,k,1.,epsilon)>space_constraint
    #if failure:
        #return 0.5 # max failure probability
    rho1,rho2=1e-8,1.
    difference=1e-8
    while abs(rho1-rho2)>difference:
        rho_tmp=(rho1+rho2)/2.
        space_tmp=compute_space(big_delta,d,k,rho_tmp,epsilon)
        if space_tmp>space_constraint:
            rho1=rho_tmp
        else:
            rho2=rho_tmp
    return rho2
# end def find_rho

def compute_space(big_delta,d,k,rho,epsilon):
    # compute space necessary given the
    # parameters of the coreset
    L = math.ceil(log(big_delta))
    grid_space=d
    O = d*L
    pi_space=O*(L+1)
    K = (2.+math.exp(1))*(L+1.)*k/rho \
            - 24.*(d**4)*((L+1.)**3)*k/(epsilon**2)*math.log(rho)
    epsilon_prime = epsilon*math.sqrt(rho) \
            / math.sqrt(8.*((2.+math.exp(1))**2)*k*(d**3)*((L+1.)**3))
    hashes_space=pi_space
    # initialize ksets
    fail_prob=rho/(L+1.)
    # one kset
    ksets_space=2*K*math.log(K/fail_prob)
    # hash functions
    ksets_space+=(big_delta**d)*math.log(K/fail_prob)
    # L x |O| ksets
    ksets_space*=L*O
    # initialize heavy-hitters
    hh_n=big_delta**d
    hh_k=(math.exp(1)+2.)*(L+1.)*k/rho
    hh_epsilon=epsilon_prime
    hh_delta=rho/(L+1.)
    constant_factor=(1+4*math.log(4)-4*math.log(5))/8
    # one heavy hitter
    heavy_hitters_space=constant_factor*math.log(hh_n/hh_delta)*\
            (8*hh_k + 1/(hh_epsilon**2))
    # hash functions
    heavy_hitters_space+=2*constant_factor*math.log(hh_n/hh_delta)*(big_delta**d)
    # L+1 heavy hitters
    heavy_hitters_space*=(L+1)
    return grid_space+pi_space+hashes_space+ksets_space+heavy_hitters_space
# end def space

class VectorIndex():
    # integerize vectors
    def __init__(self):
        self.index=dict()

    def __contains__(self,key):
        return key in self.index

    def get_index(self,vector):
        assert isinstance(vector,tuple), "vector="+str(vector)
        if vector not in self.index:
            l=len(self.index)
            self.index[vector]=l
        return self.index[vector]

    def get_vector(self,index):
        assert isinstance(index,int), "index="+str(index)
        for vector in self.index:
            if self.index[vector]==index:
                return vector

class GridStructure():
    def __init__(self,big_delta,d):
        # following notation of Braverman 2017
        is_power_of_2=big_delta==2**int(log(big_delta))
        assert is_power_of_2
        self.v=[]
        self.delta=big_delta # side length of G_0
        for _ in range(d):
            self.v.append(int(random.random()*big_delta))
        self.v=[0]*len(self.v)

    def get_center(self,ii,q):
        # ii is an integer, the level
        # q is a list representing a vector
        # the output is the center of the cell
        # containing q at level ii
        step_size=self.delta/(2**ii)
        # translate into grid
        r=[a-b for a,b in zip(q,self.v)]
        center_tmp=[int(math.floor(coef/step_size))*step_size for coef in r]
        # translate back
        center=[a+b for a,b in zip(center_tmp,self.v)]
        return center

    def get_father(self,c,ii):
        # c is a cell center at level ii
        # return the center of the cell
        # at the level just above it
        assert ii>=0
        if ii==0:
            return tuple([0]*len(self.v))
        if ii==1:
            return tuple(self.v)
        step=self.delta/(2**ii)
        assert step>0, str(self.delta)+"   "+str(ii)+"   "+str(2**ii)
        c_tmp=[float(_a-_b)/step for _a,_b in zip(c,self.v)]
        father_tmp=[int(math.floor(_n/2)) for _n in c_tmp]
        father=[_c*2*step + _v for _c,_v in zip(father_tmp,self.v)]
        return tuple(father)

    def get_children(self,c,ii):
        # get all centers of cells included in the cell
        # whose level is ii and center is c
        step=self.delta/(2**(ii+1))
        # translate c
        r = [_a-_b for _a,_b in zip(c,self.v)]
        a = [r_k for r_k in r]
        b = [r_k + step for r_k in r]
        # translate back
        a = [a_k + v_k for a_k,v_k in zip(a,self.v)]
        b = [b_k + v_k for b_k,v_k in zip(b,self.v)]
        all_centers=[]
        for kk in range(2**len(self.v)):
            theta="{0:b}".format(kk)
            vec=[]
            for jj,_b in enumerate(theta):
                _v=a[jj] if _b=="1" else b[jj]
                vec.append(float(_v))
            while len(vec)<len(self.v):
                vec.insert(0,float(b[jj]))
            all_centers.append(tuple(vec))
        result=list(set(all_centers))
        assert len(result)==2**len(self.v)
        return result

    def get_child(self,c,ii,kk):
        # get center of k-th cell included in the cell
        # whose level is ii and center is c
        if ii==-1:
            sub=self.get_cell_centers(0)
            counter,child=kk,None
            for _t in itertools.product(*sub):
                child=_t
                counter-=1
                if counter==-1:
                    break
            return child
        assert kk in range(2**len(self.v))
        step=self.delta/(2**(ii+1))
        # translate c
        r = [_a-_b for _a,_b in zip(c,self.v)]
        #a = [r_k for r_k in r]
        #b = [r_k + step for r_k in r]
        # translate back
        #a = [a_k + v_k for a_k,v_k in zip(a,self.v)]
        #b = [b_k + v_k for b_k,v_k in zip(b,self.v)]
        all_centers=[]
        theta="{0:b}".format(kk)
        theta=[int(_b) for _b in theta]
        while len(theta)<len(self.v):
            theta.insert(0,0)
        vec=[]
        for jj,_b in enumerate(theta):
            _v=r[jj]+step*int(_b)+self.v[jj]
            vec.append(float(_v))
        return tuple(vec)

    def get_cell_centers(self,ii):
        # return all cell centers at level ii
        axis_subdivision=[]
        if ii==-1:
            for v_k in self.v:
                axis_subdivision.append([0])
            return axis_subdivision
            #return [tuple([0]*len(self.v))]
        step=self.delta/(2**ii)
        for v_k in self.v: # for each dimension
            inf=-v_k/step
            sup=(self.delta-v_k)/step + 1
            # all the possible values for coordinates of cell centers
            # along that dimension, at level ii
            tmp=[n*step for n in range(inf,sup)]
            axis_subdivision.append([a+v_k for a in tmp])
        return axis_subdivision

class CoreSet():
    # following notation of Braverman 2017
    def __init__(self,big_delta,d,k,rho,epsilon,space_constraint=None, hash_constant=None):
        if space_constraint is not None:
            rho=find_rho(big_delta,d,k,epsilon,space_constraint)
            print("failure probability: " + str(rho))
        space=compute_space(big_delta,d,k,rho,epsilon)
        print("space used: " + str(space))
        assert isinstance(big_delta, int) and (big_delta > 0),\
            "Delta="+str(big_delta)
        assert isinstance(d, int) and (d > 0), "d="+str(d)
        assert isinstance(k, int) and (k > 0), "k="+str(k)
        assert (epsilon >= 0) and (epsilon <= 0.5), "epsilon="+str(epsilon)
        assert (rho >= 0) and (rho <= 1.), "rho="+str(rho)
        if hash_constant is not None:
            print("hash constant: " + str(hash_constant))
        self.vector_index=VectorIndex() # used to map vectors to indices
        # big_delta is the size of one dimension of the euclidean space
        # we assume big_delta is a power of 2
        self.grid_structure=GridStructure(big_delta,d)
        self.k=k
        self.rho=rho
        self.d=d
        self.delta=big_delta
        self.L = int(math.ceil(log(big_delta)))
        self.O = [2**i for i in \
                range(d*self.L)]
        self.pi = dict() # this may be abusive. Consider nested lists.
        # initialize pi. Will be indexed (o,i) instead of _{i}(o)
        for o in self.O:
            for ii in range(-1,(self.L)+1):
                a = 3.*((self.L+1.)**2)*big_delta*(d**2) \
                        / ((2**ii)*(epsilon**2)*o)
                b = math.log(2*(big_delta**(k*d))*(self.L+1)) \
                        -math.log(rho)
                self.pi[(o,ii)]=min(a*b,1)
                if hash_constant is not None:
                    self.pi[(o,ii)]=min(hash_constant/((2**i)*o*(epsilon**2)),1)
        self.K = (2.+math.exp(1))*(self.L+1.)*k/rho \
                - 24.*(d**4)*((self.L+1.)**3)*k/(epsilon**2)*math.log(rho)
        self.K = int(math.ceil(self.K)) # size parameter
        self.epsilon_prime = epsilon*math.sqrt(rho) \
                / math.sqrt(8.*((2.+math.exp(1))**2)*k*(d**3)*((self.L+1.)**3))
        self.m = 0
        self.hashes=dict()
        # initialize ksets
        self.ksets=dict()
        fail_prob=rho/(self.L+1.)
        for key in self.pi:
            # it makes sense to initialize them all
            # because they will all be used
            self.ksets[key]=KSet(self.K,fail_prob)
        # initialize heavy-hitters
        hh_n=big_delta**d
        hh_k=int(math.ceil((math.exp(1)+2.)*(self.L+1.)*k/rho))
        hh_epsilon=self.epsilon_prime
        hh_delta=rho/(self.L+1.)
        self.heavy_hitters=[HeavyHitter(hh_n,hh_k,hh_epsilon,hh_delta) \
                for _ in range(self.L+1)]
    # end def __init__

    def process(self,op,q): # subroutine of Update
        # op=1 if Insert, -1 if Delete
        self.m+=op
        for ii in range(self.L+1):
            c=self.grid_structure.get_center(ii,q)
            # integerize c, so that kset can process it
            v=self.vector_index.get_index(tuple(c))
            #self.heavy_hitters[ii].process(v,op)
            for o in self.O:
                self.hashes.setdefault((o,ii,v),
                        1 if random.random()<self.pi[(o,ii)] else 0)
                if self.hashes[(o,ii,v)]==1:
                    self.ksets[(o,ii)].ksetupdate(v,op)
    # end def process

    def query(self, rectify_weights=False, verbose=True):
        # 1. find o*
        o_star=None
        for o in self.O:
            if o_star is not None: # o_star found. otherwise continue
                break
            ks_o_star=[] # store k_set values, we will need them later
            for ii in range(self.L+1):
                t=self.ksets[(o,ii)].returnset()
                if t=="NIL": # means kset[(o,ii)] failed
                    break
                ks_o_star.append(list(t))
                if ii==self.L: # we got through the loop,
                    o_star=o   # so o must be o_star
        if o_star is None:
            return "FAIL"
        self.o_star=o_star
        if verbose:
            print("estimate counts")
        self.estimate_counts()
        if verbose:
            print("set light cells to zero")
        self.set_light_cells_to_zero()
        # 2. Compute weights
        if verbose:
            print("compute weights")
        self.compute_weights()
        # 3. Optionally, ensure weights are positive
        if rectify_weights:
            if verbose:
                print("rectify weights")
                print("correct counts")
            self.correct_counts()
            if verbose:
                print("compute weights")
            self.compute_weights()
        # end if rectify_weights
        S=set()
        for _x,_w in self.coreset.items():
            if _w!=0.:
                S.add((_x,_w))
        return S
    # end def query

    def get_freq(self,ind_e,hh_ind,ks_val,pi_key):
        # slightly different from the algorithm described in the paper
        # here we get directly the value of k_set, i.e. a list of tuples
        in_top_k=False
        for (x,y) in self.heavy_hitters[hh_ind].top_k:
            if x==ind_e:
                in_top_k=True
                fse=float(y)
                break
        if in_top_k:
            return fse
        in_kset=False
        for (x,y) in ks_val:
            if x==ind_e:
                in_kset=True
                fke=float(y)
                break
        if in_kset:
            return fke/self.pi[pi_key]
        return 0
    # end def get_freq

    def estimate_counts(self):
        # the counts are per level
        self.counts=dict()
        for ii in range(-1,self.L+1):
            self.counts[ii]=dict()
        self.counts[-1][tuple([0]*self.d)]=self.m
        for ii in range(self.L+1):
            things=self.ksets[(self.o_star,ii)].returnset()
            items=set(_x for _x,_ in things)
            items=items.union(_x for _x,_ in self.heavy_hitters[ii].top_k)
            for _ind in items:
                f=self.get_freq(_ind, ii,things,(self.o_star,ii))
                self.counts[ii][self.vector_index.get_vector(_ind)]=f
    # end def estimate_counts

    def set_light_cells_to_zero(self):
        for ii in range(self.L+1):
            for cell in self.counts[ii]:
                if not self.is_heavy(cell,ii):
                    self.counts[ii][cell]=0
    # end def set_light_cells_to_zero

    def compute_weights(self):
        R=dict() # dictionary mapping point to weight
        R[tuple([0]*self.d)]=self.counts[-1][tuple([0]*self.d)]
        def func1(x): # if some vector is not in the index its weight will be 0
            return x in self.vector_index
        for ii in range(self.L+1):
            for c in self.counts[ii]:
                R.setdefault(tuple(c),0)
                f=self.counts[ii].get(tuple(c),0)
                R[tuple(c)]+=f
                father=self.grid_structure.get_father(c,ii)
                R.setdefault(tuple(father),0)
                R[tuple(father)]-=f
        self.coreset=dict(R)
    # end def compute_weights

    def correct_counts(self):
        for ii in range(-1,self.L):
            self.correct_counts_level(ii)
        # end for ii
    # end def correct_counts

    def correct_counts_level(self,ii):
        items=set(_x for _x,_ in self.ksets[(self.o_star,ii)].returnset())
        items=items.union(_x for _x,_ in self.heavy_hitters[ii].top_k)
        for heavy_celli in items:
            heavy_cell=self.vector_index.get_vector(heavy_celli)
            if not self.counts[ii].get(heavy_cell,0)<0:
                continue
            if not self.is_heavy(heavy_cell,ii):
                continue
            # reduce counts of children heavy cells
            amount=abs(self.coreset[tuple(heavy_cell)])
            for kk in range(2**self.d):
                if amount==0:
                    break
                child=self.grid_structure.get_child(heavy_cell,ii,kk)
                if not self.is_heavy(child,ii+1):
                    continue
                decrease=min(amount,self.counts[ii+1][tuple(child)])
                decrease=min(decrease,self.coreset[tuple(child)])
                if decrease>0:
                    self.counts[ii+1][tuple(child)]-=decrease
                    assert self.counts[ii+1][tuple(child)]>=0
                    self.coreset[tuple(child)]-=decrease
                    assert self.coreset[tuple(child)]>=0
                    amount-=decrease
                    self.coreset[tuple(heavy_cell)]+=decrease
            # end for kk
        # end for heavy_cell
    # end def correct_counts

    def partition_coreset(self):
        # build the S_i, as defined in 4.1. and 4.2.
        self.partition=dict()
        for ii in range(-1,self.L+1):
            self.partition[ii]=set()
        for (_x,_level),_w in self.coreset.iteritems():
            if _w!=0:
                el=self.ending_level(tuple(_x))
                self.partition[el].add((tuple(_x),_level))
    # end def partition_coreset

    def is_heavy(self,cell,ii):
        # define the heavy cell identification scheme described in
        # definition 4.1. of Braverman et al.
        if ii==-1:
            return True
        # 3.
        if ii==self.L:
            return False
        # 2.
        father=self.grid_structure.get_father(cell,ii)
        if not self.is_heavy(father,ii-1):
            return False
        # 1.
        center=self.grid_structure.get_center(ii,cell)
        counts=self.counts[ii].get(tuple(center),0)
        if counts>=(2**ii)*self.rho*self.d*self.o_star/(self.k*(self.L+1)*self.delta):
            return True
        return False
    # end def is_heavy

    def ending_level(self, cell):
        ii=self.L-1
        ended=False
        while not ended:
            cell_center=self.grid_structure.get_center(ii,cell)
            cell_below=self.grid_structure.get_center(ii+1,cell)
            ended = self.is_heavy(cell_center,ii) and \
                (not self.is_heavy(cell_below,ii+1))
            ii-=1
        return ii+1

    """
    def get_children_heavy_cells(self,c,ii):
        # c is a cell center
        # ii is the level of c
        # outputs the children of c in ii+1 who are heavy
        children=self.grid_structure.get_children(c,ii) # vectors
        children=[self.vector_index.get_index(tuple(_r))\
                for _r in children] # integers
        heavies=self.heavy_hitters[ii+1].top_k # integers
        heavy_children=set(children) & set(heavies) # integers
        heavy_children=[self.vector_index.get_vector(_c)\
                for _c in list(heavy_children)] # vectors
        return heavy_children
    # end def get_children_heavy_cells

    def rectify_weights(self,S):
        for ii in range(-1,self.L):
            if ii==-1:
                center=self.grid_structure.v
                heavy_cells=[(self.vector_index.get_index(tuple(center)),
                    self.m)]
            else:
                heavy_cells=self.heavy_hitters[ii].top_k
            for (center_ind, center_weight) in heavy_cells:
                if center_weight<0:
                    center_vec=self.vector_index.get_vector[center_ind]
                    W = -center_weight
                    while W>0:
                        child=None
                        # the list might change
                        for (p_,w_) in self.get_heavy_children(center_vec,ii):
                            if w_>0:
                                child=p_
                                break
                        assert child is not None, "problem in rectify_weights:\
                            no weight to decrease found"
                        self.heavy_hitters[ii+1].process(child,-1)
                        W-=1
                    # end while W>0
            # end for (center_ind, center_weight)
        # end for ii
    # end def rectify_weights
    """
