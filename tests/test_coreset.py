from coreset import CoreSet
import time
import random

fixedseed=24011993
random.seed(fixedseed)


bdelta=4
d=2
k=1
rho=0.5
epsilon=0.5
start=time.clock()
cs=CoreSet(bdelta,d,k,rho,epsilon)
stop=time.clock()
print(stop-start)
cs.process(1, (2,2,0,1))
cs.process(1, (2,2,0,1))
print(cs.grid_structure.v)
print('1')
print(cs.grid_structure.get_cell_centers(1))
s=cs.query()
print(s)
cs.rectify_weights(s)
