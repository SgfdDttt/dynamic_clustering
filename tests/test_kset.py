from k_set import KSet,TestSingleton
import random

fixedseed=24011993
random.seed(fixedseed)

ks=KSet(2,0.000000000001)

print(ks.hashes)
for ii in range(10000):
    ks.ksetupdate(1,1)
    ks.ksetupdate(ii % 5,1)
print(ks.returnset())
