from coreset import CoreSet
import argparse
import time
import random

fixedseed=24011993
random.seed(fixedseed)#+time.time())

parser = argparse.ArgumentParser()
parser.add_argument("--stream",
            help="file containing the stream, one data point per line")
parser.add_argument("--outputfile",
            help="file to write the coreset to", default="coreset.txt")
parser.add_argument("--delta",
        help="bound on the coefficients of the points",type=int)
parser.add_argument("--k",
        help="the number of clusters",type=int)
parser.add_argument("--rho",
        help="probability of failure",type=float)
parser.add_argument("--epsilon",
        help="accuracy of algorithm",type=float)
parser.add_argument("--progress",
        help="print out progress",type=int)
args = parser.parse_args()

stream = open(args.stream,'r')
# get dimensionality from first point
first_point=stream.readline().strip('\n').split(' ')
d=len(first_point)
# initialize coreset
cs=CoreSet(args.delta,d,args.k,args.rho,args.epsilon)#,space_constraint=10e25)
cs.process(1,[int(s_) for s_ in first_point])
# go through points
counter=0
start=time.time()
for line in stream:
    counter+=1
    if (args.progress is not None) and (counter%args.progress==0):
        stop=time.time()
        print("# counter="+str(counter)+', time ' + str(stop-start) + 's')
        start=time.time()
    point=[int(s_) for s_ in line.strip('\n').split(' ')]
    cs.process(1, point)
# close file and output subset
stream.close()
# produce coreset and write to file
print("query coreset")
start=time.time()
s=cs.query(True)
stop=time.time()
print("took " + str(stop-start) + "s")
if s=='FAIL':
    print('FAIL')
else:
    outfile=open(args.outputfile,'w')
    for x,w in s:
        #outfile.write(str(y) + '\t' + str(l) + '\t' + ' '.join(str(c_) for c_ in x) + '\n')
        assert w!=0.
        outfile.write(str(w) + '\t' + ' '.join(str(c_) for c_ in x) + '\n')
    outfile.close()
