from k_set import KSet,TestSingleton
from heavy_hitter import HeavyHitter
import random

fixedseed=24011993
random.seed(fixedseed)

hh=HeavyHitter(5,2,0.1,0.1)

tups=[(34,584,330),(432,5,432),(234,423,542),(24,4,24),(243,42,432)]
index=dict((w,i) for i,w in enumerate(tups))

hh.process(index[tups[1]])
print(hh.estimate(index[tups[1]]))
print("====")
hh.process(index[tups[1]],op=-1)
print(hh.estimate(index[tups[1]]))
print("====")
for ii in range(10000):
    hh.process(index[tups[1]])
    hh.process(index[tups[ii % 5]])
print("====")
for ii in range(5):
    print(hh.estimate(index[tups[ii]]))
print('=====')
print(hh.top_k)
