from coreset import CoreSet
from kmedian_clustering import KMedian
import random


GAMMA=0.7
alphabet=['e', 'f', 'g', 'h', 'i', 'n', 'o', 'r', 's', 't', 'u', 'v', 'w', 'x', 'z']
car2int=dict((w,i) for i,w in enumerate(alphabet))
gold_dictionary=["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "zero", "zero"]

def prob(s):
    if len(s)==0:
        return 0.
    return (1-GAMMA)*(GAMMA**(len(s)-1))

def str2vec(s):
    vec=[0]*len(alphabet)
    for _c in s:
        vec[car2int[_c]]+=1
    return vec

Delta=4
dim=len(alphabet)
k=11
rho=0.01
epsilon=0.1
hash_constant=0.001
cs=CoreSet(Delta,dim,k,rho,epsilon,hash_constant=hash_constant)
print("stream text")
counter=0
for line in open("small_digits.txt","r"):
    counter+=1
    if counter%1000==0:
        print(counter)
    buff=""
    for _c in line.strip('\n'):
        buff+=_c
        if random.random()<prob(buff):
            cs.process(1, str2vec(buff))
            buff=""
    if len(buff)>0:
        cs.process(1, str2vec(buff))

print("query coreset")
s=cs.query(rectify_weights=True)
"""
for _x,_y in s:
    print(_y,_x)
"""
print("clustering")
kmedian=KMedian(k)
seeds=range(10)
cost=kmedian.multiple_fits(s, 10, seeds)
print("final clustering cost: " +str(cost))
for _m in kmedian.clusters:
        print(_m)
for _m in kmedian.clusters:
    distances=[sum((_a-_b)**2 for _a,_b in zip(_m,str2vec(_s))) for _s in gold_dictionary]
    print(distances.index(min(distances)))
