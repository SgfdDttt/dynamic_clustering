import sys
import math
import itertools

def euclidean_distance(p,q):
    assert len(p)==len(q), "len(p)="+str(len(p))+';len(q)='+str(len(q))
    return math.sqrt(sum((a-b)**2 for a,b in zip(p,q)))

def set_distance(p,Z):
    # Z is a list of points
    distances=[euclidean_distance(p,z) for z in Z]
    return min(distances)

def kmedian_cost(P,Z):
    # P and Z are 2 lists of points
    # Z are the centers
    return sum(set_distance(p,Z) for p in P)

def coreset_cost(S,Z):
    # P is a list of points
    # S is a list of (points,weight)
    return sum(s[1]*set_distance(s[0],Z) for s in S)

def compare_costs(stream_file,coreset_file,num_steps=5):
    print('read files')
    P=[]
    for line in open(stream_file,'r'):
        point=line.strip('\n').split(' ')
        P.append([float(_p) for _p in point])
    S=[]
    for line in open(coreset_file,'r'):
        point=line.strip('\n').split('\t')[1].split(' ')
        weight=line.strip('\n').split('\t')[0]
        S.append(([float(_p) for _p in point],float(weight)))
    d=len(P[0])
    print('set up grid of medians')
    # compute 1-median cost for many possible medians
    axis_subdivision=[]
    for ii in range(d):
        m=min(_p[ii] for _p in P)
        M=max(_p[ii] for _p in P)
        step=float(M-m)/(num_steps-1)
        axis_subdivision.append([m+step*jj for jj in range(num_steps)])
    all_medians=list(itertools.product(*axis_subdivision))
    print('number of configurations: ' + str(len(all_medians)))
    cost_differences=[]
    kmedian_costs=[]
    print('compute all costs')
    for median in all_medians:
        relative_error=abs(coreset_cost(S,[median])/kmedian_cost(P,[median])-1)
        kmedian_costs.append(kmedian_cost(P,[median]))
        cost_differences.append(relative_error)
    print("cost differences")
    print(len(cost_differences))
    print(min(cost_differences))
    print(max(cost_differences))
    print(sum(cost_differences)/float(len(cost_differences)))
    print("kmedian cost")
    print(len(kmedian_costs))
    print(min(kmedian_costs))
    print(max(kmedian_costs))
    print(sum(kmedian_costs)/float(len(kmedian_costs)))

compare_costs(sys.argv[1],sys.argv[2],num_steps=10)
